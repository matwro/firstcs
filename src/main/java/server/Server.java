package server;

import java.io.*;
import java.net.*;
import java.time.Duration;
import java.time.LocalDateTime;
import org.json.simple.JSONObject;

public class Server {
    private static LocalDateTime startTime = LocalDateTime.now();

    public static void main(String[] args) throws IOException {
        int port = 3009;
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("server.Server is running on port " + port);

        try {
            while (true) {
                Socket socket = serverSocket.accept();
                new ServerThread(socket).start();
            }
        } finally {
            serverSocket.close();
        }
    }

    private static class ServerThread extends Thread {
        private Socket socket;

        public ServerThread(Socket socket) {
            this.socket = socket;
        }

        public void run() {
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    System.out.println("Received command: " + inputLine);
                    JSONObject response = handleCommand(inputLine);
                    System.out.println("Sending response: " + response.toJSONString());
                    out.write(response.toJSONString() + "\n");
                    out.flush();
                    if ("stop".equalsIgnoreCase(inputLine)) {
                        in.close();
                        out.close();
                        socket.close();
                        System.exit(0);
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private JSONObject handleCommand(String command) {
            JSONObject obj = new JSONObject();
            switch (command.toLowerCase()) {
                case "uptime":
                    Duration uptime = Duration.between(startTime, LocalDateTime.now());
                    long days = uptime.toDays();
                    long hours = uptime.toHours() % 24;
                    long minutes = uptime.toMinutes() % 60;
                    long seconds = uptime.getSeconds() % 60;
                    String formattedUptime = String.format("%d days, %d hours, %d minutes, %d seconds", days, hours, minutes, seconds);
                    obj.put("uptime", formattedUptime);
                    break;
                case "info":
                    obj.put("version", "0.1.0");
                    obj.put("creation_date", "2024-05-12");
                    break;
                case "help":
                    obj.put("uptime", "Returns the server uptime." );
                    obj.put("info", "Returns the server version and creation date.");
                    obj.put("help", "Returns the list of available commands.");
                    obj.put("stop", "Stops the server and the client.");
                    break;
                case "stop":
                    obj.put("message", "Stopping server and client.");
                    break;
                default:
                    obj.put("error", "Unknown command");
                    break;
            }
            return obj;
        }
    }
}