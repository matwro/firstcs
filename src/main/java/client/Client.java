package client;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.*;

public class Client {
    public static void main(String[] args) throws IOException {
        String hostName = "localhost";
        int portNumber = 3009;

        try (Socket socket = new Socket(hostName, portNumber);
             BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
             BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))) {

            System.out.println("You are connected to server: " + hostName);
            System.out.print("Enter command: ");


            String userInput;
            while ((userInput = stdIn.readLine()) != null) {
                out.write(userInput + "\n");
                out.flush();
                String response = in.readLine();
                printFormattedResponse(response);
                if ("stop".equalsIgnoreCase(userInput)) {
                    break;
                }
                System.out.print("Enter command: ");
            }
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                    hostName);
            System.exit(1);
        }
    }
    private static void printFormattedResponse(String jsonResponse) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject jsonObject = (JSONObject) parser.parse(jsonResponse);
            jsonObject.forEach((key, value) -> {
                System.out.println(key + ": " + value);
            });
        } catch (ParseException e) {
            System.err.println("Unable to parse JSON: " + e.getMessage());
        }
    }
}